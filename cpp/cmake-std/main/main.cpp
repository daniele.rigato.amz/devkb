#include <iostream>
#include "Version.h"
#include <boost/regex.hpp>
#include <modulex/g.hpp>

using namespace std;
using namespace boost;

int main(){
  static const boost::regex e("(\\d)*");
  cout << "234 is a number? " << regex_match("234", e) <<"\n";
  cout << "abc is a number? " << regex_match("abc", e) <<"\n"; 
  cout << "bella!" << " v" << version << "\n";
  cout << cmakestd::g(3) << "\n";
  return 0;
}


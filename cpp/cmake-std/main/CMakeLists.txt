configure_file (
  "Version.h.in"
  "${PROJECT_BINARY_DIR}/gen/Version.h"
)
include_directories(${PROJECT_BINARY_DIR}/gen)
include_directories(${PROJECT_SOURCE_DIR}/modulex/inc)

#Executables
add_executable(cmake-p1 main.cpp)
if(SkipExt)
  message("skipping external project add_dependencies")
else()
  add_dependencies(cmake-p1 googletest boost)
endif()
add_dependencies(cmake-p1 modulex)
target_link_libraries(cmake-p1 gtest libboost_regex.a modulex)


# Install
install(TARGETS cmake-p1
  RUNTIME DESTINATION ${PROJECT_BINARY_DIR}/bin
  ARCHIVE DESTINATION ${PROJECT_BINARY_DIR}/lib
  LIBRARY DESTINATION ${PROJECT_BINARY_DIR}/lib)
# cmake-std

C++ / CMake standard project
* supporting multiple modules (generated libraries)
* including external libraries form both git repo and downloaded tar.gz with CMake ExternalProject
* tests with googletests

## build instructions

mkdir -p build; cd build; cmake ..; make


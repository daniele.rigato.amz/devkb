# devkb

developer knowledge-base - not a real project but a collection of recipes for various languages / frameworks / toolkits / technologies

## code organization

root
|_ language 1
 |_ framework a specific to language 1
 |_ cross-language topic x for language 1
 |_ style recommendations for language 1
|_ language 2
 |_ cross-language topic x for language 2
 ...
|_ cross-language topic 1
...



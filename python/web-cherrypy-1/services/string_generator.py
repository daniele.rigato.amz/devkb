import cherrypy
import random
import string

@cherrypy.expose
@cherrypy.tools.accept(media='application/json')
@cherrypy.tools.json_in()
@cherrypy.tools.json_out()
class WebService(object):

    def GET(self):
        return {'mystring':cherrypy.session.get('mystring', 'empty')}

    def POST(self):
        length = cherrypy.request.json["length"]
        some_string = ''.join(random.sample(string.hexdigits, length))
        cherrypy.session['mystring'] = some_string
        return {'mystring': some_string}

    def PUT(self):
        jin = cherrypy.request.json
        cherrypy.session['mystring'] = jin["another_string"]
        return {'mystring':cherrypy.session['mystring']}

    def DELETE(self):
        cherrypy.session.pop('mystring', None)
        return {'mystring':"empty"}


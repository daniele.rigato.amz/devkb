import cherrypy
import random
import string
import os
import logging
import pkgutil

import services

class WebsiteApp(object):
    #http://127.0.0.1:8080/python_generated_page?length=3
    @cherrypy.expose
    def python_generated_page(self, length=8):
        return """<html>
          <head></head>
          <body>
            <h1>Random String</h1>
            <p>this is a python generated page (html code embedded in python)</p>
            <p>random string of specified length: {}</p>
            <p><a href="/">back to index</a></p>
          </body>
        </html>""".format(''.join(random.sample(string.hexdigits, int(length))))

    #http://127.0.0.1:8080/generateLen?length=3
    @cherrypy.expose
    def generate(self, length=8):
        return ''.join(random.sample(string.hexdigits, int(length)))


if __name__ == '__main__':
    logging.info("Configuring website app")

    public_path = os.path.join(os.path.abspath(os.getcwd()), "public");
    logging.info("serving public path: %s", public_path)

conf = {
    '/': {
        'tools.sessions.on': True,
        'tools.staticdir.root': public_path,
        'tools.staticdir.on': True,
        'tools.staticdir.dir': '',
        'tools.staticdir.index': 'index.html'
    }
}

webapp = WebsiteApp()
webapp.api = lambda: None
service_modules = [modname for importer, modname, ispkg in
                   pkgutil.walk_packages(path=services.__path__, onerror=lambda x: None)]
for service_module in service_modules:
    m = __import__( "services."+service_module )
    m = getattr( m, service_module )
    m = getattr( m, "WebService" )
    conf["/api/"+service_module] = {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.response_headers.on': True,
        'tools.response_headers.headers': [('Content-Type', 'text/plain')],
    }
    setattr(webapp.api, service_module, m())
    logging.info("loaded web service " + service_module)

cherrypy.quickstart(webapp, '/', conf)
